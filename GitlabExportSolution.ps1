$solutionName ="TestSolution"
$SolutionFilePath="C:\Users\asharmila\Desktop\GitlabPS\powerautomate_rpa"
$CRMSourceUserName="sharmilaA@M365x510324.onmicrosoft.com"
$CRMSourcePassword="Comics@22"
$CRMSourceUrl="https://org4c8b6d8b.crm.dynamics.com"

Set-StrictMode -Version latest
function InstallRequiredModule{
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass -Force
$moduleName = "Microsoft.Xrm.Data.Powershell"
$moduleVersion = "2.7.2"
if (!(Get-Module -ListAvailable -Name $moduleName )) {
Write-host "Module Not found, installing now"
$moduleVersion
Install-Module -Name $moduleName -MinimumVersion $moduleVersion -Force
}
else
{
Write-host "Module Found"
}
}
function EstablishCRMConnection{
param(
[string]$CRMUserName,
[string]$CRMSecPasswd,
[string]$crmUrl)
Write-Host "UserId: $CRMUserName Password: $CRMSecPasswd CrmUrl: $crmUrl"
$CRMSecPasswdString = ConvertTo-SecureString -String $CRMSecPasswd -AsPlainText -Force
write-host "Creating credentials"
$Credentials = New-Object System.Management.Automation.PSCredential ($CRMUserName, $CRMSecPasswdString)
write-host "Credentials object created"
write-host "Establishing crm connection next"
$crm = Connect-CrmOnline -Credential $Credentials -ServerUrl $CrmUrl -ForceOAuth -Verbose
write-host "Crm connection established"
return $crm
}

InstallRequiredModule

#Update Source CRM instance details below:
Write-Host "going to create source connection"
$CrmSourceConnectionString = EstablishCRMConnection -CRMUserName "$CRMSourceUserName" -CRMSecPasswd "$CRMSourcePassword" -crmUrl "$CRMSourceUrl"
Write-Host "source connection created"
Set-CrmConnectionTimeout -conn $CrmSourceConnectionString -TimeoutInSeconds 1000

Write-Host "Publishing Customizations in source environment"
Publish-CrmAllCustomization -conn $CrmSourceConnectionString
Write-Host "Publishing Completed in source environment."

Write-Host "Exporting Solution"

Export-CrmSolution -conn $CrmSourceConnectionString -SolutionName "$solutionName" -SolutionFilePath "$SolutionFilePath" -SolutionZipFileName "$solutionName.Zip"

Write-host "Solution Exported."
